import React from 'react';
import './App.css';
import Auth from './components/Auth/Auth';
import Test from './components/Test/Test';

function App() {
  return (
    <div className="App">
      <Auth />
      {/* <h3>test</h3>
      <Test testProps={ {id:"456"} }/> */}
    </div>
  );
}

export default App;
