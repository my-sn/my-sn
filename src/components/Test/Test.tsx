import { TestModel } from "../../models"

interface TestProps {
    testProps: TestModel
}

const Test = (props: TestProps)=>{
    return(
        <div>
            <h2>
                {props.testProps.id}
            </h2>
        </div>
    )
}

export default Test