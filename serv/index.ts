import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.use(express.json())

app.get('/', (req: Request, res: Response) => {
  res.status(200).send('Express + TypeScript Server');
});

app.post('/', (req: Request, res: Response)=>{
  console.log(req.body);
  res.sendStatus(200);
})

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});